<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Default Controller
 *
 * If you wanna change the default controller, change on application/config/routes.php file
 *
 * @link http://bitbucket.org/myara/myci
 * @copyright Copyright (c) 2015, Rodrigo Borges <http://rodrigoborges.info>
 */
class Welcome extends MY_Controller 
{
    public function index() 
    {
        $this->dataView['example'] = 'Example dataview string';
        $this->bladeView('welcome', $this->dataView);
    }

}