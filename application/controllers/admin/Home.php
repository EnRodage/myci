<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Default Admin Controller
 *
 * If you wanna change the default controller, change admin route on application/config/routes.php file
 *
 * @link http://bitbucket.org/myara/myci
 * @copyright Copyright (c) 2015, Rodrigo Borges <http://rodrigoborges.info>
 */
class Home extends MY_Controller 
{
    public function index()
    {
        $this->bladeView('admin/home', $this->dataView);
    }

}