<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Login Controller
 *
 * @link http://bitbucket.org/myara/myci
 * @copyright Copyright (c) 2015, Rodrigo Borges <http://rodrigoborges.info>
 */
class Login extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model', 'user');
    }

    public $viewData = array('title' => 'Login Page');

    /**
     * Login Page
     */
    public function index()
    {
        echo $this->blade->view()->make('login/login_page', $this->viewData)->render();
    }

    /**
     * Login request
     * 
     * @return redirect to admin page or login page if fails
     */
    public function login()
    {

        if (!$this->input->post() || (!$this->input->post('username') && !$this->input->post('password') ))
        {
            $this->load->view('errors/html/error_general', array('heading' => 'Erro', 'message' => 'User and password are required'));
            return;
        }

        $this->db->limit(1);
        $qryUser = $this->user->get_by(array('user_login' => $this->input->post('username')));

        if (!$qryUser)
        {
            $this->load->view('errors/html/error_general', array('heading' => 'Erro', 'message' => "User or password are incorrect"));
            return;
        }

        $qryPass = password_verify($this->input->post('password'), $qryUser->user_password);

        if ($qryPass)
        {
            $this->session->set_userdata(array('logged' => TRUE, 'user' => $qryUser));
            redirect('admin');
        }
    }

    /**
     * Logout request
     */
    public function logout()
    {

        if ($this->session->userdata('logged'))
        {
            $this->session->sess_destroy();
        }

        redirect('admin/login');
    }

}