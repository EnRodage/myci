<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Settings controller
 *
 * This controller have only function yet, change the language
 *
 * @link http://bitbucket.org/myara/myci
 * @copyright Copyright (c) 2015, Rodrigo Borges <http://rodrigoborges.info>
 */
class Settings extends MY_Controller 
{
    /**
     * Change to specific language
     * 
     * @param String $newLang Language code, ex: en-US
     * @return redirect to referer URL
     */
    public function language_change($newLang)
    {
        $this->session->set_userdata('lang', $newLang);
        redirect($_SERVER['HTTP_REFERER']);
    }
    
}