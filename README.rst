############################
MyCI
############################

Made with CodeIgniter_ and `Blade Templating`_ (`forked to work with CodeIgniter`_)


*************
Installation 
*************

Copy this project to your host, edit the `application/config/database.php` file to your database credentials and
execute migrate controller (via terminal) to import the database.
Change directory of `application/cache` to blade can write cache files (ex. 775)

*******************
Server Requirements
*******************

PHP version 5.4 or newer is recommended.

It should work on 5.2.4 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

*******
License
*******

`GNU General Public License, version 2`_ 

*********
Resources
*********

-  `MyCI Docs`_
-  `CodeIgniter User Guide`_
-  `Blade Template`_
-  `CodeIgniter Base Model Instructions`_
-  `Twitter Bootstrap`_

Report security issues and/or bugs to our `Issues Panel`_

.. _CodeIgniter : http://www.codeigniter.com
.. _`Blade Templating`: http://laravel.com/docs/5.0/templates
.. _`forked to work with CodeIgniter`: https://bitbucket.org/rbm0407/ciblade
.. _`GNU General Public License, version 2`: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
.. _`CodeIgniter User Guide`: http://www.codeigniter.com/docs
.. _`Blade Template` : http://laravel.com/docs/5.0/templates
.. _`CodeIgniter Base Model Instructions` : https://github.com/jamierumbelow/codeigniter-base-model
.. _`Twitter Bootstrap` : http://getbootstrap.com/
.. _`Issues Panel` : https://bitbucket.org/myara/myci/issues
.. _`MyCI Docs` : http://myci.myara.net/docs
